classdef ATCDiffusionSAF < NonCooperativeSAF
    % NonCooperativeLMS
    
    properties
    end
    
    methods
        
        function obj = ATCDiffusionSAF(name, t, mem, step_sizes_weights, step_sizes_spline, splineAF)
            obj = obj@NonCooperativeSAF(name, t, mem, step_sizes_weights, step_sizes_spline, splineAF);
        end
        
        function [obj, e] = adapt(obj, X, d)

            obj = obj.start_timer();
            
            e = zeros(obj.L, 1);
            spans = zeros(obj.L, 4);

            for k = 1:obj.topology.N

                % Compute pre-output
                s = X(k, :)*obj.get_weights(k);
                % Evaluate spline
                [y, u, uIndex, g] = obj.act_fun{k}.compute_value(s);
                % Compute error
                e(k) = d(k) - y;

                dx = obj.act_fun{k}.compute_derivative(y, u, uIndex);
                ee = e(k)*dx;
                
                obj = obj.update_weights(k, obj.get_weights(k) + (obj.step_sizes_weights(k)*ee*X(k, :)'));
            
                e_av = obj.step_sizes_spline(k)*e(k);
                spans(k, :) = uIndex : uIndex + 3;
                obj.act_fun{k}.af_value(spans(k, :)) = obj.act_fun{k}.af_value(spans(k, :)) + (e_av*g');

            end
            
            new_afs_values = (obj.topology.W*cell2mat(cellfun(@(x)x.af_value', obj.act_fun, 'UniformOutput', false)))';
            obj.local_weights = (obj.topology.W*obj.local_weights);
            
            for k = 1:obj.topology.N
                obj.act_fun{k}.af_value(spans(k, :)) = new_afs_values(spans(k, :), k);
            end
            
            obj = obj.stop_timer();

        end
        
    end
    
end

