% MAKE_PLOTS

% General properties for the figures
width = 3.45;                   % Width in inches
height = 2.6;                   % Height in inches
font_size = 8;                  % Fontsize
font_size_leg = 6;              % Font size (legend)
font_name = 'TimesNewRoman';    % Font name
line_width = 1;                 % LineWidth
marker_size = 6;                % Marker size

N_scripts = length(output_scripts);

fprintf('\tRunning output scripts: ');
textprogressbar(' ');
for ii = 1:N_scripts
    textprogressbar(ii*100/N_scripts);
    eval(output_scripts{ii});
end
textprogressbar(100);
textprogressbar(' Done');