classdef ActivationFunction
    % ACTIVATIONFUNCTION - Linear or non-linear activation function
    %   This is an abstract class for representing scalar functions, which
    %   can be used for generating a dataset or internally to a filter.
    %
    %   The main methods provided by this class are:
    %
    %       - compute_value: compute f(s) for a given scalar s.
    %       - compute_derivative: compute f'(s) for a given scalar s.
    %       - init: randomly initialize the parameters of the function.
    %       - get_parameters: returns a vector of internal parameters of
    %       the function.
    %
    %   Additionally, this class provides a 'plot' method for plotting the
    %   function.
    
    properties
    end
    
    methods(Abstract)

        % Compute the value of the function
        x = compute_value(obj, s);          
        % Compute the derivative of the function
        dx = compute_derivative(obj, s);
        % Initialize the parameters
        obj = init(obj);          
        % Returns the parameters
        p = get_parameters(obj);            
        
    end
    
    methods
        
        function obj = ActivationFunction()
        end
        
        function plot(obj, limit, resolution, color, line_width, marker_number, marker_type, marker_size)
            % Plot the function on the currently active figure
            %   Parameters:
            %   
            %   - limit (required): limit for plotting.
            %   - resolution (optional): resolution of the x-axis (default 
            %   to 500).
            %   - color (optional): color of the line. Can be a string 
            %   specification, or a 3-valued vector specifying a RGB color.
            %   Default to 'r'.
            %   - line_width: width of the line, default to 1.
            
            if(nargin < 8)
                marker_size = 6;
            end
            
            if(nargin < 7)
                marker_type = '';
            end
            
            if(nargin < 6)
                marker_number = 0;
            end
            
            if(nargin < 5)
                line_width = 1;
            end
            
            if(nargin < 4)
                color = 'r';
            end
            
            if(nargin < 3)
                resolution = 500;
            end
            
            dx = (limit(2)-limit(1))/resolution;
            xx = limit(1);
            x1 = zeros(resolution, 1);
            yy2 = zeros(resolution, 1);
            for k = 1:resolution
                yy2(k) = obj.compute_value(xx);   % Adapted
                x1(k) = xx;
                xx = xx + dx;
            end
            if(ischar(color))
                if(marker_number == 0)
                    plot(x1, yy2, color, 'LineWidth', line_width, 'MarkerSize', marker_size);
                else
                    line_fewer_markers(x1, yy2, marker_number, color, 'LineWidth', line_width, 'MarkerSize', marker_size);
                end
            else
                if(marker_number == 0)
                    plot(x1, yy2, 'LineWidth', line_width, 'Color', color, 'MarkerSize', marker_size);
                else
                    line_fewer_markers(x1, yy2, marker_number, marker_type, 'LineWidth', line_width, 'Color', color, 'MarkerSize', marker_size);
                end
            end
            
        end
        
    end
    
end

