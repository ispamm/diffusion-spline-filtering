% Initial settings
clc; clear all; close all;
addpath(genpath(pwd));
RandStream.setGlobalStream(RandStream('mt19937ar','seed',531));

% -------------------------------------------------------------------------
% --- CONFIGURATION -------------------------------------------------------
% -------------------------------------------------------------------------

N_agents = 5;                           % Number of agents in the network             
M = 5;                                  % Memory of the filters
N_samples = 50000;                      % Number of (local) samples
runs = 15;                              % Repetitions
mu = rand(N_agents, 1)*0.01;           % Step sizes for w
mu_spline = rand(N_agents, 1)*0.01;    % Step sizes for spline

% Define the topology of the network. See the folder 'network' for all the
% possible choices.
t = RandomTopology(N_agents, 'metropolis', 0.6);

% Define the spline
splineAF = SplineAF(2, 0.2, 'catmulrom');
af = splineAF.init();

% Filters to be tested
filters = { ...
    NonCooperativeLMS('NC-LMS',t, M, mu), ...
    CTADiffusionLMS('D-LMS', t, M, mu), ...
    NonCooperativeSAF('NC-SAF',t, M, mu, mu_spline, splineAF), ...
    CTADiffusionSAF('CTA-SAF', t, M, mu, mu_spline, splineAF), ...
};

% Script to be executed in output (see the folder 'plot').
output_scripts = { ...
    'plot_errors', ...
    'plot_local_errors', ...
    'plot_dataset_statistics', ...
    'plot_afs', ...
    'plot_output_weights', ...
    'plot_mu', ...
};
limit_plot = [-2 2];

% -------------------------------------------------------------------------
% --- SIMULATION ----------------------------------------------------------
% -------------------------------------------------------------------------

% Define the output structures
N_filters = length(filters);
MSE = zeros(N_agents, N_samples, N_filters);
MSD = zeros(N_agents, N_samples, N_filters);
MSD_nonlin = zeros(N_agents, N_samples, N_filters);
t_times = zeros(N_filters, 1);

% Save the original filters
filters_o = filters;

% Compute which filters are the same as the model (for the non-linear MSD)
MSD_nonlin_idx = false(N_filters, 1);
for k = 1:N_filters
    if(~isempty(filters{k}.act_fun) && strcmp(class(filters{k}.act_fun{1}), class(af)))
        MSD_nonlin_idx(k) = true;
    end
end

for L=1:runs % iterating over experiments
    
    fprintf('--------------------------------------------------------------\n');
    fprintf('--- RUN %i/%i -------------------------------------------------\n', L, runs);
    fprintf('--------------------------------------------------------------\n');
    
    % Reset the filters
    filters = filters_o;
    
    % Generate the dataset for the current run
    [ X, Y, dconfig ] = generate_dataset( N_samples, M, N_agents, af );
    
    fprintf('\tFiltering progress: ');
    textprogressbar(' ');
    
    for i=1:N_samples  % iterating over time
       
       % Eventually print on screen the progress
       if(mod(i, 50) == 0)
           textprogressbar(i*100/N_samples);
       end

       % Extract the current sample
       if(N_agents > 1)
           uk = squeeze(X(:, i, :));
       else
           uk = squeeze(X(:, i, :))';
       end
       dk = Y(:, i);
       
       for f = 1:N_filters
           % Update filter
           [filters{f}, e_tmp] = filters{f}.adapt(uk, dk);
           % Update mean-square error
           MSE(:, i, f) = MSE(:, i, f) + e_tmp.^2; 
           for k = 1:N_agents
               % Update MSD (linear)
               MSD(k, i, f) = MSD(k, i, f) + norm(dconfig.wo - filters{f}.get_weights(k));
               % Update MSD (non-linear)
               if(MSD_nonlin_idx(f))
                   MSD_nonlin(k,i,f) = MSD_nonlin(k,i,f) + norm(dconfig.af.get_parameters() - filters{f}.act_fun{k}.get_parameters());
               end
           end
       end
             
    end
    
    % Update times
    t_times_tmp = cellfun(@(x) mean(x.adapt_time/N_filters, 1), filters);
    t_times = t_times + t_times_tmp(:);
    
    textprogressbar(100);
    textprogressbar(' Done.');
    fprintf('--------------------------------------------------------------\n\n');
   
end

% Normalize the error and the training times
MSE = MSE/runs;
MSD = MSD/runs;
MSD_nonlin = MSD_nonlin/runs;
t_times = t_times/runs;

MSE_db = 10*log10(MSE);
MSD_db = 10*log10(MSD);
MSD_nonlin_db = 10*log10(MSD_nonlin);

MSE_av = squeeze(sum(MSE, 1))/N_agents;
MSD_av = squeeze(sum(MSD, 1))/N_agents;
MSD_nonlin_av = squeeze(sum(MSD_nonlin, 1))/N_agents;

MSE_av_db = 10*log10(MSE_av);
MSD_av_db = 10*log10(MSD_av);
MSD_nonlin_av_db = 10*log10(MSD_nonlin_av);

if(N_agents > 1)
    % Visualize the network
    t.visualize();
end
% Execute the output scripts
fprintf('\n--------------------------------------------------------------\n');
fprintf('--- RESULTS --------------------------------------------------\n');
fprintf('--------------------------------------------------------------\n');
make_plots;

% Plot error and training times
e_int = 100;
fprintf('\tFinal errors and training times:\n');
if(N_filters > 1)
    disptable([mean(MSE_av_db(end-100:end, :))' mean(MSD_av_db(end-100:end, :))'  mean(MSD_nonlin_av_db(end-100:end, :))' t_times], ...
         {'MSE', 'MSD', 'MSD (nonlinear)', 'Time'}, ...
         cellfun(@(x) x.name, filters, 'UniformOutput', false));
else
    disptable([mean(MSE_av_db(end-100:end))' mean(MSD_av_db(end-100:end))'  mean(MSD_nonlin_av_db(end-100:end))' t_times], ...
         {'MSE', 'MSD', 'MSD (nonlinear)', 'Time'}, ...
         cellfun(@(x) x.name, filters, 'UniformOutput', false));
end
fprintf('\b-------------------------------------------------------------\n');

