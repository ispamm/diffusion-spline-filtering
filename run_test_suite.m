
% RUN_TEST_SUITE Run all the test suite

% License to use and modify this code is granted freely without warranty to all, as long as the original author is
% referenced and attributed as such. The original author maintains the right to be solely associated with this work.
%
% Programmed and Copyright by Simone Scardapane:
% simone.scardapane@uniroma1.it

addpath(genpath(pwd));
if verLessThan('matlab', '8.1')
    error('This Matlab version does not support unitary testing. To run the test suite, please upgrade to version 8.1 or higher.');
end

import matlab.unittest.TestSuite;
clc;

p = 0; f = 0;

tic;
% Open the tests in the folder
fold = 'tests';
suiteFolder = TestSuite.fromFolder(fold);
result = run(suiteFolder);

fprintf('\n');
cprintf('*text', 'Tests for folder %s: \n', fold);

for i=1:length(result)
    
    if(result(i).Passed)
        fprintf('\n');
        cprintf('comment', '\t Test %s: PASSED', result(i).Name);
    else
        fprintf('\n');
        cprintf('err', '\t Test %s: NOT PASSED', result(i).Name);
    end
end

fprintf('\n\n\n');

for i=1:length(result)
    p = p + result(i).Passed;
    f = f + result(i).Failed;
end

fprintf('Total passed tests: %i.\nTotal failed tests: %i.\n', p, f);
fprintf('Total testing time: %.2f secs\n', toc);