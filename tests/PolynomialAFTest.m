classdef PolynomialAFTest < matlab.unittest.TestCase
    
    methods (Test)

        function testFirstOrderPolynomial(testCase)
            
            p = PolynomialAF(2, 1);
            p.coefficients = 0.5;
            
            [s, q] = p.compute_value(3);
            testCase.assertEqual(s, 1.5);
            testCase.assertEqual(q, 3);
           
            s = p.compute_derivative(3);
            testCase.assertEqual(s, 0.5);
            
        end
        
        function testThirdOrderPolynomial(testCase)
            
            p = PolynomialAF(2, 3);
            p.coefficients = [1.5 0 0.5]';
            
            [s, q] = p.compute_value(4);
            testCase.assertEqual(s, 38);
            testCase.assertEqual(q, [4 16 64]);
            
            s = p.compute_derivative(4);
            testCase.assertEqual(s, 25.5);
            
        end
        
    end  
    
end

