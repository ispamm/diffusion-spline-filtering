% NetworkTopology - Abstract class for creating network topologies
%   A network topology is a graph detailing how a set of nodes is
%   interconnected. Given an implementation T, we can use it as follows.
%   First, construct the topology:
%
%       o = T(N, weights_type, varargin);
%
%   where:
%
%       * N is the size of the network.
%       * weights_type determines the weights (can be 'max_degree',
%       'metropolis').
%       * The additional parameters depends on the specific class.
%
%   Then, access the list of neighbors of node i as:
%
%       idx = T.getNeighbors(i);
%
%   You can get the edge value between i and j as:
%
%       e = T.getEdgeValue(i, j);
%
%   Finally, plot the topology as:
%
%       T.visualize();
%
%   [This requires the Bioinformatics toolbox.]
%
%   Any class implementing NetworkTopology must implement the internal
%   method construct() to create the topology. This method is called up to
%   25 times, until a connected topology is created.
%
%   Note that network topologies are required to be undirected and
%   connected.

% License to use and modify this code is granted freely without warranty to all, as long as the original author is
% referenced and attributed as such. The original author maintains the right to be solely associated with this work.
%
% Programmed and Copyright by Simone Scardapane:
% simone.scardapane@uniroma1.it

classdef NetworkTopology
    
    properties
        N; % Number of nodes
        W; % Weight matrix
        A; % Adjacency matrix
        weights_type; % Weights type (string)
    end
    
    methods
        function obj = NetworkTopology(N, weights_type, varargin)
            % Construct the NetworkTopology object
            obj.N = N;
            obj.W = zeros(obj.N, obj.N);
            obj.A = zeros(obj.N, obj.N);
            obj.weights_type = weights_type;
        end

        function obj = init(obj)
            % Initialize the topology. Remember to call this method in your
            % constructor!
            obj.A = obj.buildAdjacency();
            counter = 1;
            while(~(obj.isConnected()))
                if(counter > 25)
                    error('Failed to initialize a connected network topology');
                end
                obj.A = obj.buildAdjacency();
                counter = counter + 1;
            end
            % Set the weights
            obj.W = obj.buildWeights(obj.weights_type);
        end
        
        function visualize(obj, t)
            % Plot the graph
            % The optional value is the title of the plot
            
            if(nargin < 2)
                t = 'Graph viewer';
            end
            % Plot the graph (need the Bioinformatics toolbox)
            if(exist('biograph', 'file') == 2)
                % Very complicated hack to set the title and shift the
                % figures
                b = biograph(sparse(triu(obj.A)));
                b.showArrows = 'off';
                view(b);
                set(0, 'ShowHiddenHandles', 'on');
                bgfig = gcf;
                position = get(bgfig, 'Position');
                c = get(bgfig, 'Children');
                h = figure;
                copyobj(c(1), h);
                set(h, 'Name', t);
                set(h, 'Units', 'points');
                set(h, 'Position', position);
                close(bgfig);
            end
        end
        
        function idx = getNeighbors(obj, i)
            % Get neighbors indexes of i-th node
            idx = find(obj.A(i, :) == 1);
        end
        
        function v = getEdgeValue(obj, i, j)
            % Get edge value between nodes i and j
            v = full(obj.W(i, j));
        end
        
        function b = isConnected(obj)
            % Check if the graph is connected
            S = graphconncomp(sparse(obj.A), 'Weak', true, 'Directed', false);
            b = S == 1;
        end
        
        function d = getMaxDegree(obj)
            % Get the maximum degree
            d = max(sum(obj.A));
        end
        
        function d = getDegree(obj, i)
            % Get the degree of node i
            if(nargin == 2)
                d = sum(obj.A(i, :));
            else
                d = sum(obj.A);
            end
        end

        
        function W = buildWeights(obj, weights_type)
            % Set the weights matrix W based on the adjacency matrix A
            
            W = zeros(obj.N, obj.N);
            max_degree = obj.getMaxDegree();
            d = obj.getDegree();
            
            for ii = 1:obj.N
                idx = obj.getNeighbors(ii);
                for jj = idx
                    if(strcmp(weights_type, 'max_degree'))
                        W(ii, jj) = 1/(max_degree + 1);
                    elseif(strcmp(weights_type, 'metropolis'))
                        W(ii, jj) = 1/max([d(ii), d(jj)]);
                    end
                end
                if(strcmp(weights_type, 'max_degree'))
                    W(ii, ii) = 1-length(idx)/(max_degree+1);
                elseif(strcmp(weights_type, 'metropolis')) 
                    W(ii, ii) = 1 - sum(W(ii, :));
                end
            end
        end
    end
    
    methods(Abstract)
        % Construct the adjacency matrix A.
        A = buildAdjacency(obj);
        
        % Get description
        s = getDescription(obj);
    end
    
   
end

