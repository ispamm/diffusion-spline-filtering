%% Datasets

figure('name', 'Dataset statistics');
figshift;
subplot(1,2,1);
plot(dconfig.sigma_v2, 'o-', 'MarkerSize', marker_size, 'LineWidth', line_width);
title('Sigma', 'FontSize', font_size, 'FontName', font_name);
box on;
grid on;
xlabel('Node', 'FontSize', font_size, 'FontName', font_name);
ylabel('\sigma^2', 'Interpreter', 'tex', 'FontSize', font_size, 'FontName', font_name);

subplot(1,2,2);
plot(dconfig.a, 'o-', 'MarkerSize', marker_size, 'LineWidth', line_width);
title('a', 'FontSize', font_size, 'FontName', font_name);
box on;
grid on;
xlabel('Node', 'FontSize', font_size, 'FontName', font_name);
ylabel('a_k', 'Interpreter', 'tex', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend();

width = 2*width;
singlecolumn_format;
width = width/2;