function [ X, Y, dconfig ] = generate_dataset( N_samples, M, L, af )
% GENERATE_DATASET - Construct local datasets
%   This functions generates local datasets at every node. The input is 
%   given by a recursive process defined by:
%
%       x[n+1] = a*x[n] + sqrt(1-a^2)*v[n]                              (1)
%
%   where v[n] is Gaussian noise with fixed variance, and 'a' is a
%   correlation coefficient. Variances and correlation coefficients are
%   extracted randomly at every node from a uniform probability
%   distribution, such that 'a' is in [0,0.8], and the variance in
%   [-10,-35] (in dB). The output is given by f(w'*x), where 'x' is a
%   buffer of the last 'M' elements, the weights 'w' are extracted randomly
%   from a normal distribution, and f() is given as parameter.
%
%   Input parameters:
%
%       - N_samples: number of samples at every node.
%       - M: memory of the filter.
%       - L: number of agents in the network.
%       - af: non-linear function in Eq. (1). Must be of class
%       'ActivationFunction'.
%
%   Output parameters:
%
%       - X: L x N_samples x M tensor of input values.
%       - Y: L x N_samples matrix of output values.
%       - dconfig: struct containing the non-linear function, the linear 
%       weights ('wo'), noise and autocorrelation terms.
%
%   See also: ACTIVATIONFUNCTION.

% Save the activation function
dconfig.af = af;

% Generate the linear weights
dconfig.wo = randn(M,1);
dconfig.wo = dconfig.wo / norm(dconfig.wo,2);

% Generate local correlation coefficients and noise terms
dconfig.a = rand(L, 1)*0.8;
dconfig.sigma_v2_dB = rand(L,1)*10-35;
dconfig.sigma_v2 = 10.^(dconfig.sigma_v2_dB/10);

% Initialize progress bar
fprintf('\tGenerating data: ');
textprogressbar(' ');

% Initialize data structures
X = zeros(L, N_samples, M);
Y = zeros(L, N_samples);

for agent = 1:L
    
    % Update progress bar
    textprogressbar((agent-1)*100/L);
    
    % Compute the input signal
    ak = dconfig.a(agent);
    b = sqrt(1 - ak^2);
    x = filter( b, [1 -ak], randn(N_samples, 1)); % H(z) = b/(1+a*z^-1) ( Eq. (60) )
    
    % Compute the noise term
    dn = sqrt(dconfig.sigma_v2(agent)) * randn(N_samples, 1);
    
    % Initialize the input buffer
    x_buff = zeros(M, 1);

    for n = 1:N_samples
        
        % Update the buffer
        x_buff = [x(n); x_buff(1:end-1)];
        X(agent, n, :) = x_buff';
        
        % Compute the output
        Y(agent, n) = dconfig.af.compute_value(x_buff'*dconfig.wo) + dn(n);
        
    end
    
end
textprogressbar(100);
textprogressbar(' Done');

end