classdef PolynomialAF < ActivationFunction
    % POLYNOMIALAF - Polynomial function
    %   This represents a polynomial of a fixed order, given by:
    %       f(s) = a1*s^P + a2*s^P-1 + ... + aP*s                       (1)
    
    properties
        order;          % Order of the polynomial
        coefficients;   % Coefficients
    end
    
    methods
        
        function obj = PolynomialAF(order)
            % Construct the polynomial function
            obj = obj@ActivationFunction();
            obj.order = order;
            obj.coefficients = zeros(obj.order, 1);
            obj.coefficients(1) = 1;
        end
        
        function [x, g] = compute_value(obj, s)
            
            g = s.^[1:obj.order];
            x = g*obj.coefficients;
            
        end
        
        function dx = compute_derivative(obj, s)
            
            g = (1:obj.order).*[1 s.^[1:obj.order-1]];
            dx = g*obj.coefficients;
            
        end
        
        function obj = init(obj)
            obj.coefficients = rand(obj.order, 1)*2 - 1;
        end
        
        function p = get_parameters(obj)
            p = obj.coefficients;
        end

    end
    
end

