Diffusion Spline Adaptive Filtering

Description
-------
This code implements a diffused version of the spline adaptive filter [1],
for continuous nonlinear adaptation over a network of agents. With respect to 
standard linear approaches to distributed filtering [2,3], it provides nonlinear
modeling capabilities with a small computational overhead. The paper describing the
algorithm has been published in [4].


Citation
-------
If you use this code or any derivative thereof in your research, I would appreciate
if you cite the original paper as:

@inproceedings{scardapane2016diffusion,
  title={Diffusion Spline Adaptive Filtering},
  author={Scardapane, Simone and Scarpiniti, Michele and Comminiello, Danilo and Uncini, Aurelio},
  booktitle={2016 24th European Signal Processing Conference (EUSIPCO)},
  pages={1498--1502},
  year={2016}
}


Usage 
-------
To launch a simulation, simply use the script 'run_simulation.m'. All the
configuration parameters are specified at the top of the script. Four models
are compared:

   * Non cooperative and diffusion LMS.
   * Non cooperative and diffusion SAF.

The code is highly flexible, and it is possible to define and add many elements
to the simulation, including:

   * New filters: all the available filters are defined in the 'filters' folder,
     and they derive from the abstract class 'DiffusionFilter'. You can modify
     which algorithms to include in the simulation by modifying the 'filters'
     array in the main script (line 26).

   * Network topologies: all the possible network topologies (random, linear, etc.)
     are available in the 'network' folder. You can select a new topology on line
     19 of the main script, or implement a new one by specializing the
     'NetworkTopology' class.

   * Plotting functions: these are defined in the 'plot' folder. You can add
     new ones to the simulation by modifying the 'output_scripts' cell array
     on line 34.

The unitary testing suite in the 'tests' folder is currently under 
active development.


Licensing
---------
The code is distributed under BSD-2 license. Please see the file called LICENSE.

The code uses several utility functions from MATLAB Central. Copyright
information and licenses can be found in the 'utils' folder.

Network topology in folder 'network' is adapted from the Lynx MATLAB toolbox:
https://github.com/ispamm/Lynx-Toolbox


Contacts
--------

   o If you have any request, bug report, or inquiry, you can contact
     the author at simone [dot] scardapane [at] uniroma1 [dot] it.
   o Additional contact information can also be found on the website of
     the author:
	      http://ispac.ing.uniroma1.it/scardapane/


References
--------
[1] Scarpiniti, M., Comminiello, D., Parisi, R., & Uncini, A. (2013). 
    Nonlinear spline adaptive filtering. Signal Processing, 93(4), 772-783.
[2] Lopes, C. G., & Sayed, A. H. (2008). Diffusion least-mean squares over 
    adaptive networks: Formulation and performance analysis. IEEE Transactions 
    on Signal Processing, 56(7), 3122-3136.
[3] Sayed, A. H. (2014). Adaptation, learning, and optimization over networks. 
    Foundations and Trends in Machine Learning, 7(4-5), 311-801.
[4] Scardapane, S., Scarpiniti, M., Comminiello, D. and Uncini, A. (2016). 
    Diffusion Spline Adaptive Filtering. In 2016 24th European Signal Processing 
    Conference (EUSIPCO), 1498-1502. Eurasip.