classdef LinearAF < ActivationFunction
    % LinearAF - Linear activation function
    %   This represents a generic linear function f(s) = ms + q. When
    %   initialized, this is simply the identity function f(s) = s.
    
    properties
        m;  % Slope coefficient
        q;  % Bias coefficient
    end
    
    methods
        
        function obj = LinearAF()
            % Construct the linear function
            obj = obj@ActivationFunction();
            obj.m = 1;
            obj.q = 0;
        end
        
        function x = compute_value(obj, s)
            
            x = obj.m*s + obj.q;
            
        end
        
        function dx = compute_derivative(obj, ~)
            
            dx = obj.m;
            
        end
        
        function obj = init(obj)
            obj.m = rand();
            obj.q = rand();
        end
        
        function p = get_parameters(obj)
            p = [obj.m; obj.q];
        end
        
    end
    
end

