%% Datasets

figure('name', 'Step sizes');
figshift;
subplot(1,2,1);
plot(mu, 'o-', 'MarkerSize', marker_size, 'LineWidth', line_width);
title('Step size (linear)', 'FontSize', font_size, 'FontName', font_name);
box on;
grid on;
xlabel('Node', 'FontSize', font_size, 'FontName', font_name);
ylabel('\mu_w', 'Interpreter', 'tex', 'FontSize', font_size, 'FontName', font_name);

subplot(1,2,2);
plot(mu_spline, 'o-', 'MarkerSize', marker_size, 'LineWidth', line_width);
title('Step size (non-linear)', 'FontSize', font_size, 'FontName', font_name);
box on;
grid on;
xlabel('Node', 'FontSize', font_size, 'FontName', font_name);
ylabel('\mu_q', 'Interpreter', 'tex', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend();

width = 2*width;
singlecolumn_format;
width = width/2;