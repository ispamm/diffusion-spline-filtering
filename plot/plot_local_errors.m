
%% Individual errors

cmap = hsv(N_agents);
[bb, aa] = butter(2, 0.02 );

leg = cell(N_agents, 1);
f_mse = figure('name', 'Local MSEs');
figshift;

f_msd = figure('name', 'Local MSD (linear)');
figshift;

if(any(MSD_nonlin_idx))
    f_msd_nl = figure('name', 'Local MSD (non-linear)');
    figshift;
    z = 1;
end

for ii = 1:N_filters
    
    set(0, 'currentfigure', f_mse);
    subplot(2, ceil(N_filters/2), ii);
    hold on;
    box on;
    grid on;
    
    for k = 1:N_agents
        plot(filter(bb,aa, MSE_db(k, :, ii)),'LineWidth', line_width, 'Color', cmap(k, :));
        leg{k} = sprintf('Node %i', k);
    end
    
    xlabel('Sample', 'FontSize', font_size, 'FontName', font_name);
    ylabel('MSE', 'FontSize', font_size, 'FontName', font_name);
    title(sprintf('Filter %s', filters{ii}.name),'FontSize', font_size, 'FontName', font_name);
    h_legend = legend(leg, 'Location', 'NorthEast');
    
    set(0, 'currentfigure', f_msd);
    subplot(2, ceil(N_filters/2), ii);
    hold on;
    box on;
    grid on;
    
    for k = 1:N_agents
        plot(filter(bb,aa, MSD_db(k, :, ii)),'LineWidth', line_width, 'Color', cmap(k, :));
    end
    
    xlabel('Sample', 'FontSize', font_size, 'FontName', font_name);
    ylabel('MSE', 'FontSize', font_size, 'FontName', font_name);
    title(sprintf('Filter %s', filters{ii}.name),'FontSize', font_size, 'FontName', font_name);
    h_legend = legend(leg, 'Location', 'NorthEast');
    
    if(MSD_nonlin_idx(ii))
        set(0, 'currentfigure', f_msd_nl);
        subplot(2, ceil(sum(MSD_nonlin_idx)/2), z);
        hold on;
        box on;
        grid on;

        for k = 1:N_agents
        	plot(filter(bb,aa, MSD_nonlin_db(k, :, ii)),'LineWidth', line_width, 'Color', cmap(k, :));
        end

        xlabel('Sample', 'FontSize', font_size, 'FontName', font_name);
        ylabel('MSD (non-linear)', 'FontSize', font_size, 'FontName', font_name);
        title(sprintf('Filter %s', filters{ii}.name),'FontSize', font_size, 'FontName', font_name);
        h_legend = legend(leg, 'Location', 'NorthEast');
        
        z = z + 1;
    end
    
end
