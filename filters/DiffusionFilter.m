classdef DiffusionFilter
    % DIFFUSIONFILTER Abstract class for diffusion filters
    %   This class can be used as a baseline for implementing generic
    %   diffusion filters. Given a generic implementation T, initialize the
    %   filter as:
    %
    %   o = T(name, topology, M, varargin);
    %
    %   where:
    %       - name is the name of the filter (to be displayed in the
    %       results).
    %       - topology is the topology of the network (object of class
    %       NetworkTopology).
    %       - M is the local buffer size (common to each filter).
    %       - varargin are additional parameters dependent on T.
    %
    %   A single adaptation step is called as:
    %
    %       [obj, e] = o.adapt(x, d);
    %
    %   where x is the input buffer of size LxM (where L is the size of the
    %   network), and d the L-dimensional vector of desired responses. The
    %   output argument e is an L-dimensional vector of a-priori local
    %   errors.
    %
    %   Other methods provided by the class are:
    %
    %       - get_weights: returns the local linear output weights.
    %       - update_weights: update the local linear output weights.
    %       - start_timer/stop_timer: start and stop the timers for storing
    %       the adaptation time.
    %
    %   See also: ACTIVATIONFUNCTION, NETWORKTOPOLOGY.
    
    properties
        name;           % Name of the filter
        local_weights;  % Matrix of local linear weights
        L;              % Number of agents in the network
        topology;       % Topology of the network
        adapt_time;     % Total time spent adapting            
        mem;            % Size of the input buffer
        act_fun;        % Cell array of activation functions (depending on the filter)
    end
    
    properties(Access=private)
        start_time;     % Stores the time when calling 'start_timer'
    end
    
    methods
        
        function obj = DiffusionFilter(name, t, mem)
            % Construct the diffusion filter
            obj.name = name;
            obj.mem = mem;
            obj.topology = t;
            obj.L = t.N;
            obj.local_weights = zeros(obj.L, mem);
            obj.adapt_time = zeros(obj.L, 1);
            obj.act_fun = [];
        end
        
        function w = get_weights(obj, ii)
            % Get the local linear weights
            w = obj.local_weights(ii, :)';
        end
        
        function obj = update_weights(obj, ii, new_w)
            % Updates the local linear weights
            obj.local_weights(ii, :) = new_w;
        end
        
        function obj = start_timer(obj)
            % Start the timer
            obj.start_time = clock;
        end
        
        function obj = stop_timer(obj)
            % Stop the timer
            obj.adapt_time = obj.adapt_time + etime(clock, obj.start_time);
        end
        
    end
    
    methods(Abstract)
        % Local adaptation (see help of DiffusionFilter)
        [obj, e] = adapt(obj, x, d);   
    end
    
end

