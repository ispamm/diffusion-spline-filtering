% RandomTopology - Random graph
%   A random graph G(N, p) is a graph with N nodes, where each edge has
%   probability p of being present (also known as Erd?s�R�nyi random
%   graph).
    
% License to use and modify this code is granted freely without warranty to all, as long as the original author is
% referenced and attributed as such. The original author maintains the right to be solely associated with this work.
%
% Programmed and Copyright by Simone Scardapane:
% simone.scardapane@uniroma1.it

classdef RandomTopology < NetworkTopology
    
    properties
        p;
    end
    
    methods
        function obj = RandomTopology(N, weights_type, p)
            obj = obj@NetworkTopology(N, weights_type);
            obj.p = p;
            obj = obj.init();
        end
        
        function A = buildAdjacency(obj)
            A = zeros(obj.N, obj.N);
            dice = triu(rand(obj.N, obj.N));
            dice = dice + triu(dice, 1)';
            A(dice < obj.p) = 1;
            A(logical(eye(obj.N))) = 0;
        end
        
        function s = getDescription(obj)
            s = sprintf('Random graph G(%i, %.2f)', obj.N, obj.p);
        end
    end
    
end

