
%% Output weights

cmap = hsv(N_agents);

leg = cell(1 + N_agents, 1);
leg{1} = 'Real';
f_mse = figure('name', 'Output Weights');
figshift;

for ii = 1:N_filters
    
    subplot(2, ceil(N_filters/2), ii);
    hold on;
    box on;
    grid on;
    
    plot(dconfig.wo, '-or', 'LineWidth', line_width);
    for k = 1:N_agents
        plot(filters{ii}.get_weights(k), '--x', 'LineWidth', line_width, 'Color', cmap(k, :));
        leg{k+1} = sprintf('Node %i', k);
    end
    
    xlabel('Weight Index', 'FontSize', font_size, 'FontName', font_name);
    ylabel('Value', 'FontSize', font_size, 'FontName', font_name);
    title(sprintf('Filter %s', filters{ii}.name),'FontSize', font_size, 'FontName', font_name);
    h_legend = legend(leg, 'Location', 'NorthEast');
    
end
