classdef SplineAF < ActivationFunction
    % SPLINEAF - Spline activation function.
    %   This represents a flexible spline function, using Catmul-Rom or
    %   B-Spline interpolating matrices. It is defined by a set of Q
    %   control points, which for simplicity are considered equispaced and
    %   symmetric around the origin. The value at a point is obtained by a
    %   suitable interpolation of a 3rd order polynomial around the 4
    %   closest control points. More information can be obtained on a
    %   number of publications, including:
    %
    %   [1] Scarpiniti, M., Comminiello, D., Parisi, R., & Uncini, A. 
    %   (2013). Nonlinear spline adaptive filtering. Signal Processing, 
    %   93(4), 772-783.
    %   [2] Wahba, G. (1990). Spline models for observational data (Vol. 
    %   59). Siam.
    
    properties
        control_points; % Placements of the control points on the x-axis
        af_value;       % Value of the control points on the y-axis
        spline_type;    % Integer defining the interpolating matrix
        sampling_step;  % Sampling step (only used during initialization)
        C;              % Interpolating matrix
    end
    
    methods
        
        function obj = SplineAF(limit, sampling_step, spline_type)
            % Construct the spline function.
            % Parameters are:
            %   - limit: the spline is defined in [-limit, +limit].
            %   - sampling_step: displacement between two consecutive
            %   control points.
            %   - spline_type: type of interpolating matrix, 'catmulrom' or
            %   'bspline'.
            
            obj = obj@ActivationFunction();
            
            % Define the control points
            obj.control_points = -limit - sampling_step:sampling_step:limit + sampling_step;
            
            % Require a column vector
            obj.control_points = obj.control_points(:);
            obj.sampling_step = sampling_step;
            
            % Convert the string argument to an integer argument
            if (strcat(spline_type, 'catmulrom'))
                obj.spline_type = 1;
                obj.C = obj.get_C();
            else
                obj.spline_type = 2;
                obj.C = obj.get_C();
            end
            
            % Initialize the spline as the identity function
            obj.af_value = obj.control_points;
            
        end
        
        function [x, u, uIndex, g] = compute_value(obj, s)
            % Compute the value of the spline
            %   Additional output arguments are:
            %   - u: normalized abscissa value between closest control
            %   point and subsequent one.
            %   - uIndex: index of the closest control point.
            %   - g: coordinate vector given by [u^3 u^2 u^1 1].
            
            [u, uIndex] = obj.get_index(s);
            g = [u^3 u^2 u 1]*obj.C;
            x = g*obj.af_value(uIndex : uIndex+3);
            
        end
        
        function [dx, u, uIndex, g] = compute_derivative(obj, s, u, uIndex)
            % Compute the derivative of the spline.
            %   Additional output arguments are:
            %   - u, uIndex: identical to compute_value(s).
            %   - g: derivative coordinate vector given by [3u^2 2u 1 0].
            %   It is possible to provide u and uIndex as additional
            %   arguments to speed up the computation, if they have already
            %   been computed previously.
            
            if(nargin < 3)
                [u, uIndex] = obj.get_index(s);
            end
            g  = [3*u^2 2*u 1 0]*obj.C;
            dx = g*(obj.af_value( uIndex : uIndex + 3 ))/obj.sampling_step;
            
        end
        
        function [u, uIndex] = get_index(obj, s)
            % Return u and uIndex from s (see description of
            % compute_value).
            
            np = length(obj.control_points);
            Su = s/obj.sampling_step + (np-1)/2;
            uIndex =  floor(Su);
            u = Su-uIndex;
            if uIndex<1 % the index must start from 1
                uIndex = 1;
            end
            if uIndex>(np-3),
                uIndex = np-3;
            end
        end
        
        function C = get_C(obj)
            % Return the interpolating matrix.
            
            if (obj.spline_type == 1)
                C = 0.5*[-1  3 -3  1; ...
                    2 -5  4 -1; ...
                    -1  0  1  0; ...
                    0  2  0  0];
            elseif (obj.spline_type == 1)
                C = (1/6)*[-1  3 -3  1; ...
                    3 -6  3  0; ...
                    -3  0  3  0; ...
                    1  4  1  0];
            end
        end

        function obj = init(obj)
            % TODO
%             obj.af_value = [
%                 -2.20
%                 -2.00
%                 -1.80
%                 -1.60
%                 -1.40
%                 -1.20
%                 -1.00
%                 -0.80
%                 -0.91
%                 -0.40
%                 0.20
%                 -0.05
%                 0.00
%                 -0.15
%                 0.58
%                 1.00
%                 1.00
%                 1.20
%                 1.40
%                 1.60
%                 1.80
%                 2.00
%                 2.20
%                 ];
obj.af_value = [
                -2.20
                -2.00
                -1.62
                -1.3
                -1.40
                -1.20
                -1.00
                -0.80
                -2.12
                -0.40
                0.20
                -0.1
                1.4
                -0.15
                0.58
                1.00
                1.45
                1.20
                1.40
                1.60
                1.80
                2.00
                2.20
                ];
            
        end
        
        function p = get_parameters(obj)
            p = obj.af_value;
        end
        
    end
    
end

