

saf_idx = [];
for ii = 1:N_filters
    if(isa(filters{ii}, 'NonCooperativeSAF') || isa(filters{ii}, 'NonCooperativePAF'))
        saf_idx = [saf_idx ii];
    end
end

if(~isempty(saf_idx))
    figure('Name', 'Activation Functions');
    figshift;
end

z = 0;
for ii = saf_idx
    
    z = z + 1;
    subplot(ceil(length(saf_idx)/2), 2, z);
    hold on;
    
    cmap = hsv(N_agents);
    leg = cell(1 + N_agents, 1);
    leg{1} = 'True';
    KK = 500;
    
    dconfig.af.plot(limit_plot, KK, 'r--', line_width);
    f = filters{ii};
    for agent = 1:N_agents
        
        leg{agent+1} = sprintf('Node %i', agent);
        f.act_fun{agent}.plot(limit_plot, KK, cmap(agent, :), line_width);
        
    end
    
    title(sprintf('Filter %s', f.name),'FontSize', font_size, 'FontName', font_name);
    xlabel('Linear combiner output {\its}[{\itn}] ','FontSize', font_size, 'FontName', font_name);
    ylabel('SAF output {\ity}[{\itn}]','FontSize', font_size, 'FontName', font_name);
    box on;
    grid on;
    
    h_legend = legend(leg, 'Location', 'NorthWest');
    
end
