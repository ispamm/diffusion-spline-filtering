classdef NonCooperativeSAF < DiffusionFilter
    % NonCooperativeLMS
    
    properties
        step_sizes_weights;  % Local step sizes (weights)
        step_sizes_spline;   % Local step sizes (spline)
    end
    
    methods
        
        function obj = NonCooperativeSAF(name, t, mem, step_sizes_weights, step_sizes_spline, splineAF)
            obj = obj@DiffusionFilter(name, t, mem);
            obj.step_sizes_weights = step_sizes_weights;
            obj.step_sizes_spline = step_sizes_spline;
            obj.act_fun = cell(t.N, 1);
            obj.act_fun = cellfun(@(x) splineAF, obj.act_fun, 'UniformOutput', false);
        end
        
        function [obj, e] = adapt(obj, X, d)

            obj = obj.start_timer();
            e = zeros(obj.L, 1);
            
            for ii = 1:obj.L

                % Compute pre-output
                s = X(ii, :)*obj.get_weights(ii);
                
                % Evaluate spline
                [y, u, uIndex, g] = obj.act_fun{ii}.compute_value(s);
                
                % Compute error
                e(ii) = d(ii) - y;
                dx = obj.act_fun{ii}.compute_derivative(s, u, uIndex);
                ee = e(ii)*dx;

                % LMS weights
                obj = obj.update_weights(ii, obj.get_weights(ii) + ...
                    (obj.step_sizes_weights(ii)*ee*X(ii, :)'));

                % LMS control points
                e_av = obj.step_sizes_spline(ii)*e(ii);
                span_idx = uIndex : uIndex + 3;
                obj.act_fun{ii}.af_value(span_idx) = obj.act_fun{ii}.af_value(span_idx) + (e_av*g');

            end
            
            obj = obj.stop_timer();

        end
            
        
    end
    
end

