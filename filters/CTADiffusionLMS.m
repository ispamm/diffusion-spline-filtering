classdef CTADiffusionLMS < NonCooperativeLMS
    % DiffusionLMS
    
    properties
    end
    
    methods
        
        function obj = CTADiffusionLMS(name, t, mem, step_sizes)
            obj = obj@NonCooperativeLMS(name, t, mem, step_sizes);
        end
        
        function [obj, e] = adapt(obj, X, d)
            
            obj = obj.start_timer();
            e = zeros(obj.L, 1);
            psi = zeros(obj.mem, obj.L);
            
            for k = 1:obj.L
                e(k) = d(k) - obj.get_weights(k)'*X(k, :)';
                for l=1:obj.L
                    psi(:,k) = psi(:,k) + obj.topology.getEdgeValue(l,k)*obj.get_weights(l);
                end
            end
            
            for k = 1:obj.L
                obj = obj.update_weights(k, psi(:,k) + obj.step_sizes(k)*X(k, :)'*(d(k) - X(k, :)*psi(:,k)));
            end
            obj = obj.stop_timer();
 
        end
        
    end
    
end

