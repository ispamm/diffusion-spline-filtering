classdef NonCooperativeLMS < DiffusionFilter
    % NonCooperativeLMS
    
    properties
        step_sizes;  % Local step sizes
    end
    
    methods
        
        function obj = NonCooperativeLMS(name, t, mem, step_sizes)
            obj = obj@DiffusionFilter(name, t, mem);
            obj.step_sizes = step_sizes;
        end
        
        function [obj, e] = adapt(obj, X, d)
            
            obj = obj.start_timer();
            e = zeros(obj.L, 1);
            
            for ii = 1:obj.L
                
                e(ii) = d(ii) - obj.get_weights(ii)'*X(ii, :)';
                obj = obj.update_weights(ii, obj.get_weights(ii) + obj.step_sizes(ii)*e(ii)*X(ii, :)');
                
            end
            
            obj = obj.stop_timer();
            
        end
        
    end
    
end

