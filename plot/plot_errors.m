%% Global errors

f_mse = figure('name', 'Global MSE');
figshift;
hold on;
box on;
grid on;
xlabel('Sample', 'FontSize', font_size, 'FontName', font_name);
ylabel('MSE', 'FontSize', font_size, 'FontName', font_name);
f_msd = figure('name', 'Global MSD (linear)');
figshift;
hold on;
box on;
grid on;
xlabel('Sample', 'FontSize', font_size, 'FontName', font_name);
ylabel('MSD', 'FontSize', font_size, 'FontName', font_name);
if(any(MSD_nonlin_idx))
    f_msd_nl = figure('name', 'Global MSD (non-linear)');
    figshift;
    hold on;
    box on;
    grid on;
    xlabel('Sample', 'FontSize', font_size, 'FontName', font_name);
    ylabel('MSD (non-linear)', 'FontSize', font_size, 'FontName', font_name);
end

cmap = hsv(N_filters);
[bb, aa] = butter(2, 0.02 );

for ii = 1:N_filters
    %plot(MSE_av_db(:, ii),'LineWidth', line_width, 'Color', cmap(ii, :));
    if(N_filters == 1)
        MSE_local = MSE_av_db(1, :);
        MSD_local = MSD_av_db(1, :);
        MSD_nonlin_local = MSD_nonlin_av_db(1, :);
    else
        MSE_local = MSE_av_db(:, ii);
        MSD_local = MSD_av_db(:, ii);
        MSD_nonlin_local = MSD_nonlin_av_db(:, ii);
    end
    set(0, 'currentfigure', f_mse);
    plot(filter(bb,aa,MSE_local),'LineWidth', line_width, 'Color', cmap(ii, :));
    set(0, 'currentfigure', f_msd);
    plot(filter(bb,aa,MSD_local),'LineWidth', line_width, 'Color', cmap(ii, :));
    if(MSD_nonlin_idx(ii))
        set(0, 'currentfigure', f_msd_nl);
        plot(filter(bb,aa,MSD_nonlin_local),'LineWidth', line_width, 'Color', cmap(ii, :));
    end
end

f_names = cellfun(@(x)x.name, filters, 'UniformOutput', false);

set(0, 'currentfigure', f_mse);
h_legend = legend(f_names, 'Location', 'NorthEast');
singlecolumn_format;

set(0, 'currentfigure', f_msd);
h_legend = legend(f_names, 'Location', 'NorthEast');
singlecolumn_format;

if(any(MSD_nonlin_idx))
    set(0, 'currentfigure', f_msd_nl);
    h_legend = legend(f_names(MSD_nonlin_idx), 'Location', 'NorthEast');
    singlecolumn_format;
end