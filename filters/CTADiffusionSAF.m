classdef CTADiffusionSAF < NonCooperativeSAF
    % NonCooperativeLMS
    
    properties
        adapt_type;
    end
    
    methods
        
        function obj = CTADiffusionSAF(name, t, mem, step_sizes_weights, step_sizes_spline, splineAF)
            obj = obj@NonCooperativeSAF(name, t, mem, step_sizes_weights, step_sizes_spline, splineAF);
        end
        
        function [obj, e] = adapt(obj, X, d)

            e = zeros(obj.L, 1);

            obj = obj.start_timer();
            psiW = (obj.topology.W*obj.local_weights)';
            new_afs_values = obj.topology.W*cell2mat(cellfun(@(x)x.af_value', obj.act_fun, 'UniformOutput', false));
            new_afs = obj.act_fun;
            obj = obj.stop_timer();
            
            for k = 1:obj.topology.N
                
                % Compute pre-output
                s = X(k, :)*obj.get_weights(k);
                % Evaluate spline
                y = obj.act_fun{k}.compute_value(s);
                % Compute error
                e(k) = d(k) - y;
 
            end
            
            obj = obj.start_timer();
            for k = 1:obj.topology.N
                
                new_afs{k}.af_value = new_afs_values(k, :)';
                
                s = X(k, :)*psiW(:, k);
                [y, u, uIndex, g] = new_afs{k}.compute_value(s);
                
                e_phi = d(k) - y;
                dx = new_afs{k}.compute_derivative(y, u, uIndex);
                ee = e_phi*dx;
                
                obj = obj.update_weights(k, psiW(:, k) + (obj.step_sizes_weights(k)*ee*X(k, :)'));
            
                e_av = obj.step_sizes_spline(k)*e_phi;
                span_idx = uIndex : uIndex + 3;
                obj.act_fun{k}.af_value(span_idx) = new_afs{k}.af_value(span_idx) + (e_av*g');

            end
            obj = obj.stop_timer();
    
        end
        
    end
    
end

